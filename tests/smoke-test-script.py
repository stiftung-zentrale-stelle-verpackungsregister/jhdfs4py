"""Smoke test script to test the library in various environments
"""
from pyspark.sql import SparkSession

from jhdfs4py import HdfsFs


def main() -> None:
    spark = SparkSession.builder.getOrCreate()
    hdfs = HdfsFs.from_spark_session(spark)

    print("Listing contents of HDFS root directory:")
    for p in hdfs.listdir("/"):
        print(f"  /{p}")


if __name__ == "__main__":
    main()
