from setuptools import setup


def readme():
    with open("README.md") as f:
        return f.read()


setup(
    name="jhdfs4py",
    version_format="{tag}.dev{commitcount}+{gitsha}",
    description="Convenient HDFS access using the Java HDFS client",
    author_email="opensource@verpackungsregister.org",
    long_description=readme(),
    long_description_content_type="text/markdown",
    url="https://gitlab.com/stiftung-zentrale-stelle-verpackungsregister/jhdfs4py",
    packages=["jhdfs4py"],
    # See https://mypy.readthedocs.io/en/stable/installed_packages.html?highlight=py.typed#making-pep-561-compatible-packages
    package_data={"jhdfs4py": ["py.typed"]},
    zip_safe=False,
    python_requires=">=3.6",
    install_requires=[
        "pyspark>=2.4,<3.3",
        "dataclasses>=0.7; python_version < '3.7'",
    ],
    setup_requires=["setuptools-git-version==1.0.3"],
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: Apache Software License",
        "Operating System :: OS Independent",
    ],
)
